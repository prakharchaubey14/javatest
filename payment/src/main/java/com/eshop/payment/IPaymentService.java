package com.eshop.payment;

import com.eshop.model.payment.PaymentDetails;

public interface IPaymentService {
	/**
	 * This method is used to make payments.
	 * @return
	 */
	PaymentDetails makePayment();
}
