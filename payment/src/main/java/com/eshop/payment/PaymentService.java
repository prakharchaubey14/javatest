package com.eshop.payment;

import org.springframework.stereotype.Service;

import com.eshop.model.payment.PaymentDetails;

@Service
public class PaymentService implements IPaymentService{
	public PaymentDetails makePayment() {
		System.out.println(this.getClass().toString() + "getMakePayment()");
		PaymentDetails paymentDetails = new PaymentDetails();
		paymentDetails.setPaymentStatus("Success");
		return paymentDetails;
	}
}
