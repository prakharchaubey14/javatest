Technologies Used- Java 7, Spring MVC 4.0, Jsp/Jstl, Maven 3.1, Junit

About Architecture- The application code is highly scalable and designed in component based manner. The application contains five different projects.
1. eShoppingApp - The main project(parent of all project). It contains all projects as modules. We need to build this project in order to build war/jar(all components)
2. eShopping - This is the web project which contains view/controller.
3. common - This is a common project for all the other components. It mainly contains model & utility classes.
4. payment - This is not in use. It is created as a place holder for integration with payment gateways.
5. product - This is the main product module which contains all the business logic related to products. It contains methods to get product details, buy product etc.


Reason why you used specific frameworks and libraries for the front-end and back-end
	- For front end, i have used basic jsp/jstl. It is quick to design UI in jsp/jstl and later you can add the html/css/template. We have used Spring form tags on UI for some advanced things(like list binding with form) which traditional jsp doesn�t support. jstl library is always useful for writing some conditions/logic on UI and it works well with Spring tags/jsp.	

How the persistent layer could be implemented?
	- There is no persistence layer right now in the application. The current implementation is memory based. I would prefer to use hibernate if any relational database is going to be used as the backend. Spring provides very good support to integrate hibernate as an orm tool.
	- I can think about the other NoSQL database options to store data. If we go for any NoSQL DB then we will have to write the persistence layer in a different way(not similar to any orm tool). We will need to think about transaction and other stuff(ACID) while writing the persistence layer.

How long did it take you to create the code?
	- It took around 16-18 hours to write this code.


