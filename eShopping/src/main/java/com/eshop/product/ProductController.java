package com.eshop.product;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.eshop.model.common.ResponseMessage;
import com.eshop.model.product.BuyProduct;
import com.eshop.model.product.InventoryDetails;
import com.eshop.model.product.Item;
import com.eshop.model.product.OrderSummary;
import com.eshop.model.vo.BuyProductDetailsVO;
import com.eshop.model.vo.BuyProductVO;
import com.eshop.payment.IPaymentService;
import com.eshop.product.service.IProductService;
/**
 * Controller for product related operations.
 * @author prakh
 *
 */
@Controller
public class ProductController {
	
	@Autowired
	private IProductService productService;
	
	//TODO for payment module integration
	@Autowired
	private IPaymentService PaymentService;
	
	/**
	 * Home page request mapped method.
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public String getProductDetails(Model model) {
		InventoryDetails inventoryDetails = productService.getInventoryDetails();
		model.addAttribute("itemList", inventoryDetails.getItemList());
		
		BuyProductVO buyProductVO = populateBuyProductVO(inventoryDetails.getItemList());
		model.addAttribute("buyProductVO", buyProductVO);
		
		return "home";
	}
	
	/**
	 * populate buy product.
	 * @param list
	 * @return
	 */
	private BuyProductVO populateBuyProductVO(List<Item> list) {
		BuyProductVO buyProductVO = new BuyProductVO();
		if(!CollectionUtils.isEmpty(list)){
			for(Item item : list){
				BuyProductDetailsVO buyProductDetailsVO = new BuyProductDetailsVO();
				buyProductDetailsVO.setItemId(item.getItemId());
				buyProductDetailsVO.setItemName(item.getItemName());
				buyProductVO.getBuyProductDetails().add(buyProductDetailsVO);
			}
		}
		return buyProductVO;
	}

	/**
	 * mapped method for buy the product.
	 * @param model
	 * @param buyProductVO
	 * @return
	 */
	@RequestMapping(value="/buyProduct", method=RequestMethod.POST)
	public String buyProduct(Model model,@ModelAttribute("buyProductVO") BuyProductVO buyProductVO){
		if(!CollectionUtils.isEmpty(buyProductVO.getBuyProductDetails())){
			List<BuyProduct> buyProductList = new ArrayList<>();
			for(BuyProductDetailsVO buyProductDetailsVO : buyProductVO.getBuyProductDetails()){
				BuyProduct buyProduct = new BuyProduct();
				buyProduct.setCount(buyProductDetailsVO.getCount());
				buyProduct.setItemId(buyProductDetailsVO.getItemId());
				buyProduct.setItemName(buyProductDetailsVO.getItemName());
				buyProductList.add(buyProduct);
			}
			final ResponseMessage ResponseMessage = productService.buyProduct(buyProductList);
			if(ResponseMessage.getStatus().equalsIgnoreCase("SUCCESS")){
				OrderSummary orderSummary = new OrderSummary();
				orderSummary.setOrderSummary(buyProductList);
				model.addAttribute("orderSummary", orderSummary);
				return "summary";
			}else{
				model.addAttribute("responseMessage", ResponseMessage);
				return "error";
			}
		}
		return "error";
	}
	
}
