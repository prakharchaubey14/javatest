<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>eShopping</title>
</head>
<body>
	<h3>Currently available in our shop</h3>
	<c:set var="contextName" value='<%=request.getContextPath()%>' />
	<c:forEach items="${itemList}" var="item">
		<tr>
			<td>
				<h4>
					<c:out value="${item.itemName}" />
					:
					<c:out value="${item.itemsLeft}" />
				</h4>
			</td>
		</tr>
	</c:forEach>
	<br>
	<br>
	<br>
	<h3>Your Shopping</h3>
	<h4>
		<form:form method="post" action="${contextName}/buyProduct"
			modelAttribute="buyProductVO">
			<c:forEach items="${buyProductVO.buyProductDetails}" var="buyDetails"
				varStatus="status">
				<c:out value="${buyDetails.itemName}" /> : <input type="text"
					name="buyProductDetails[${status.index}].count" />
				<input type="hidden"
					name="buyProductDetails[${status.index}].itemId"
					value="${buyDetails.itemId}" /><br><br>
				<input type="hidden"
					name="buyProductDetails[${status.index}].itemName"
					value="${buyDetails.itemName}" />
			</c:forEach>
			<br />
			<input type="submit" name="Buy" value="Buy"></input>
		</form:form>
	</h4>


</body>
</html>