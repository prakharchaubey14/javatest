<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>eShopping</title>
</head>
<body>
	<h3>
		<a href="<%=request.getContextPath()%>/home">Home</a>
	</h3>
	<br>
	<h3>Order Summary</h3>
	<table border="1">
		<tr>
			<th>No.</th>
			<th>Item Name</th>
			<th>Count</th>
			<th>Payment Status</th>
			<th>Order Status</th>
		</tr>
		<c:set var="srno" value="1" scope="page"></c:set>
		<c:forEach items="${orderSummary.orderSummary}" var="buyProduct">
			<c:if test="${not empty buyProduct.count}">
				<tr>
					<td align="center"><c:out value="${srno}" /> <c:set
							var="srno" value="${srno+1}" scope="page"></c:set></td>
					<td><c:out value="${buyProduct.itemName}" /></td>
					<td><c:out value="${buyProduct.count}" /></td>
					<td>NA</td>
					<td>Success</td>
				</tr>
			</c:if>
		</c:forEach>
	</table>

</body>
</html>