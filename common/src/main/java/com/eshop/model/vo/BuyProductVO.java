package com.eshop.model.vo;

import java.util.ArrayList;
import java.util.List;

public class BuyProductVO {
	private List<BuyProductDetailsVO> buyProductDetails;

	public List<BuyProductDetailsVO> getBuyProductDetails() {
		if(buyProductDetails==null){
			buyProductDetails = new ArrayList<BuyProductDetailsVO>();
		}
		return buyProductDetails;
	}

	public void setBuyProductDetails(List<BuyProductDetailsVO> buyProductDetails) {
		this.buyProductDetails = buyProductDetails;
	}
	
}
