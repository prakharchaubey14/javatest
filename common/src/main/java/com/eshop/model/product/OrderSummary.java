package com.eshop.model.product;

import java.util.List;

public class OrderSummary {
	private List<BuyProduct> orderSummary;

	public List<BuyProduct> getOrderSummary() {
		return orderSummary;
	}

	public void setOrderSummary(List<BuyProduct> orderSummary) {
		this.orderSummary = orderSummary;
	}
	
}
