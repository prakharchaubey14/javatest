package com.eshop.model.product;

public class Item {
	private String itemId;
	private String itemName;
	private int itemsLeft;

	public int getItemsLeft() {
		return itemsLeft;
	}

	public void setItemsLeft(int itemsLeft) {
		this.itemsLeft = itemsLeft;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
}
