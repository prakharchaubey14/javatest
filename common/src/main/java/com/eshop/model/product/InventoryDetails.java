package com.eshop.model.product;

import java.util.ArrayList;
import java.util.List;

public class InventoryDetails {
	private List<Item> itemList;

	public List<Item> getItemList() {
		if(itemList==null){
			itemList = new ArrayList<Item>();
		}
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}
	

}
