package com.eshop.product.service;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eshop.model.common.ResponseMessage;
import com.eshop.model.product.BuyProduct;
import com.eshop.model.product.InventoryDetails;
import com.eshop.model.product.Item;
import com.eshop.product.validator.BuyProductValidator;
import com.eshop.product.validator.IBuyProductValidator;
/**
 * 
 * @author prakh
 *
 */
@Service
public class ProductService implements IProductService{

	private InventoryDetails inventoryDetails;

	@Autowired
	private IBuyProductValidator buyProductValidator;

	/**
	 * This method is used to pre populate the inventory
	 */
	@PostConstruct
	public void loadProductData() {
		System.out.println(this.getClass().toString() + "loadProductData()");
		inventoryDetails = new InventoryDetails();
		Item itemA = new Item();
		itemA.setItemsLeft(20);
		itemA.setItemId("itemA");
		itemA.setItemName("Item A");
		Item itemB = new Item();
		itemB.setItemsLeft(10);
		itemB.setItemId("itemB");
		itemB.setItemName("Item B");
		inventoryDetails.getItemList().add(itemA);
		inventoryDetails.getItemList().add(itemB);
	}
	/*
	 * (non-Javadoc)
	 * @see com.eshop.product.service.IProductService#getInventoryDetails()
	 */
	public InventoryDetails getInventoryDetails() {
		System.out
				.println(this.getClass().toString() + "getInventoryDetails()");
		return inventoryDetails;
	}
	/*
	 * (non-Javadoc)
	 * @see com.eshop.product.service.IProductService#buyProduct(java.util.List)
	 */
	public ResponseMessage buyProduct(List<BuyProduct> buyProductList) {
		System.out
				.println(this.getClass().toString() + "getInventoryDetails()");
		// calling validator
		ResponseMessage responseMessage = buyProductValidator
				.validateBuyProduct(buyProductList);

		if (responseMessage.getStatus() == null
				|| responseMessage.getStatus().isEmpty()) {
			// buy item if no status message
			for (BuyProduct buyProduct : buyProductList) {
				for (Item item : inventoryDetails.getItemList()) {
					if (buyProduct.getItemId().equalsIgnoreCase(
							item.getItemId()) && !buyProduct.getCount().isEmpty()) {
						item.setItemsLeft(item.getItemsLeft()
								- Integer.valueOf(buyProduct.getCount()));
					}
				}
			}
			responseMessage.setStatus("SUCCESS");
		}
		return responseMessage;

	}
}
