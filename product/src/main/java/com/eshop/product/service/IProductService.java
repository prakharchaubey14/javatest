package com.eshop.product.service;

import java.util.List;

import com.eshop.model.common.ResponseMessage;
import com.eshop.model.product.BuyProduct;
import com.eshop.model.product.InventoryDetails;
/**
 * Product service
 * @author prakh
 *
 */
public interface IProductService {
	/**
	 * This method return the current product details from inventory.
	 * @return
	 */
	InventoryDetails getInventoryDetails();

	/**
	 * This method is used to buy the product.
	 * @param buyProductList
	 * @return
	 */
	ResponseMessage buyProduct(List<BuyProduct> buyProductList);
}
