package com.eshop.product.validator;

import java.util.List;

import com.eshop.model.common.ResponseMessage;
import com.eshop.model.product.BuyProduct;
/**
 * 
 * @author prakh
 *
 */
public interface IBuyProductValidator {
	/**
	 * This method validates the input before buying the product.
	 * @param buyProductList
	 * @return
	 */
	ResponseMessage validateBuyProduct(List<BuyProduct> buyProductList);
}
