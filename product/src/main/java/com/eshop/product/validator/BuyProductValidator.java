package com.eshop.product.validator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.eshop.model.common.ResponseMessage;
import com.eshop.model.product.BuyProduct;
import com.eshop.model.product.InventoryDetails;
import com.eshop.model.product.Item;
import com.eshop.product.service.ProductService;
/**
 * 
 * @author prakh
 *
 */
@Component
public class BuyProductValidator implements IBuyProductValidator{

	@Autowired
	private ProductService productService;

	/*
	 * (non-Javadoc)
	 * @see com.eshop.product.validator.IBuyProductValidator#validateBuyProduct(java.util.List)
	 */
	public ResponseMessage validateBuyProduct(List<BuyProduct> buyProductList) {
		ResponseMessage responseMessage = new ResponseMessage();
		// validate input
		if (responseMessage.getStatus()==null || responseMessage.getStatus().isEmpty()) {
			validateInput(buyProductList, responseMessage);
		}
		// validate if items left
		if (responseMessage.getStatus()==null || responseMessage.getStatus().isEmpty()) {
			validateItems(buyProductList, responseMessage);
		}
		return responseMessage;
	}

	private void validateItems(List<BuyProduct> buyProductList,
			ResponseMessage responseMessage) {
		InventoryDetails inventoryDetails = productService
				.getInventoryDetails();
		for (Item item : inventoryDetails.getItemList()) {
			for (BuyProduct buyProduct : buyProductList) {
				if (!buyProduct.getCount().isEmpty() && item.getItemsLeft() < Integer
						.valueOf(buyProduct.getCount())) {
					responseMessage.setMessage("Item(s) are out of stock.");
					responseMessage.setStatus("ERROR");
				}
			}
		}
	}

	private void validateInput(List<BuyProduct> buyProductList,
			ResponseMessage responseMessage) {
		String status = null;
		String message = null;
		if (CollectionUtils.isEmpty(buyProductList)) {
			status = "ERROR";
			message = "Please enter number of items to buy";
		} else {
			boolean noItemsToBuy = true;
			for (BuyProduct buyProduct : buyProductList) {
				if (!StringUtils.isEmpty(buyProduct.getCount())) {
					noItemsToBuy = false;
					if (!isInteger(buyProduct.getCount())) {
						status = "ERROR";
						message = "Please enter correct number of items to buy(in number format)";
					}
				}
			}
			if (noItemsToBuy) {
				status = "ERROR";
				message = "Please enter number of items to buy";
			}
		}
		responseMessage.setMessage(message);
		responseMessage.setStatus(status);
	}

	private boolean isInteger(String input) {
		try {
			Integer.parseInt(input);
		} catch (NumberFormatException ne) {
			return false;
		}
		return true;
	}
}
