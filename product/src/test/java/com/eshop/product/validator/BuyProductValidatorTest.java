package com.eshop.product.validator;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.eshop.model.common.ResponseMessage;
import com.eshop.model.product.BuyProduct;
import com.eshop.test.TestConfiguration;

public class BuyProductValidatorTest extends TestConfiguration {
	@Autowired
	private IBuyProductValidator buyProductValidator;

	@Test
	public void testValidateBuyProductInvalidCount() {
		List<BuyProduct> buyProductList = new ArrayList<BuyProduct>();
		BuyProduct buyProduct = new BuyProduct();
		buyProduct.setItemId("itemA");
		buyProduct.setItemName("Item A");
		buyProduct.setCount("12d");
		buyProductList.add(buyProduct);
		ResponseMessage responseMessage = buyProductValidator
				.validateBuyProduct(buyProductList);
		assertTrue(responseMessage.getStatus().equals("ERROR"));
		assertTrue(responseMessage.getMessage().equals("Please enter correct number of items to buy(in number format)"));
	}

	@Test
	public void testValidateBuyProductBlankCount() {
		List<BuyProduct> buyProductList = new ArrayList<BuyProduct>();
		BuyProduct buyProduct = new BuyProduct();
		buyProduct.setItemId("itemA");
		buyProduct.setItemName("Item A");
		buyProduct.setCount("");
		buyProductList.add(buyProduct);
		ResponseMessage responseMessage = buyProductValidator
				.validateBuyProduct(buyProductList);
		assertTrue(responseMessage.getStatus().equals("ERROR"));
		assertTrue(responseMessage.getMessage().equals("Please enter number of items to buy"));
	}

	@Test
	public void testValidateBuyProductOutOfStock() {
		List<BuyProduct> buyProductList = new ArrayList<BuyProduct>();
		BuyProduct buyProduct = new BuyProduct();
		buyProduct.setItemId("itemA");
		buyProduct.setItemName("Item A");
		buyProduct.setCount("89");
		buyProductList.add(buyProduct);
		ResponseMessage responseMessage = buyProductValidator
				.validateBuyProduct(buyProductList);
		assertTrue(responseMessage.getStatus().equals("ERROR"));
		assertTrue(responseMessage.getMessage().equals("Item(s) are out of stock."));
	}
	
	@Test
	public void testValidateBuyProductSuccess() {
		List<BuyProduct> buyProductList = new ArrayList<BuyProduct>();
		BuyProduct buyProduct = new BuyProduct();
		buyProduct.setItemId("itemA");
		buyProduct.setItemName("Item A");
		buyProduct.setCount("5");
		buyProductList.add(buyProduct);
		ResponseMessage responseMessage = buyProductValidator
				.validateBuyProduct(buyProductList);
		assertTrue(responseMessage.getStatus()==null); 
		assertTrue(responseMessage.getMessage()==null);
	}

}
